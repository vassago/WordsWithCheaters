
public class Letters {
	private static Letters l;
	private Letter[] letters;
	
	private Letters(){
		letters=new Letter[27];
		char c='a';		
		for(int x=0;x<26;x++){			
			if(c=='u'||c=='n'||c=='d'||c=='l'){
				letters[x]=new Letter(c,2);
			}else
			if(c=='g'||c=='h'||c=='y'){
				letters[x]=new Letter(c,3);
			}else
			if(c=='b'||c=='c'||c=='f'||c=='m'||c=='p'||c=='w'){
				letters[x]=new Letter(c,4);
			}else
			if(c=='k'||c=='v'){
				letters[x]=new Letter(c,5);
			}else
			if(c=='x'){
				letters[x]=new Letter(c,8);
			}else
			if(c=='j'||c=='q'||c=='z'){
				letters[x]=new Letter(c,10);
			}else
				letters[x]=new Letter(c,1);
			
			c++;
		}
		letters[26]=new Letter(' ',0);
	}
	
	public static Letters letterGen(){
		if(l==null){
			return new Letters();
		}else{
			return l;
		}
	}
	
	public Letter[] getLetters(){
		return letters;
	}
	
	public class Letter{
		char c;
		int val;
		
		public Letter(char c,int val){
			this.c=c;
			this.val=val;			
		}
		
		public char getChar(){
			return c;
		}
		public int getVal(){
			return val;
		}
	}
}
