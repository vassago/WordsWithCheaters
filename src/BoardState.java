import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * BoardState holds the data and state of the current grid. This 
 * class keeps track of what coordinates have been visited as well as
 * the current word being built.
 * 
 * @author Richard Thatcher
 *
 */
public class BoardState {
	public final int WIDTH=15,HEIGHT=15;
	public Tile[][] board;
	private String word;
	private Letters letters;

	public BoardState(){
		board=initBoard();
		letters=Letters.letterGen();
	}

	public BoardState(String filename){
		board=readState(filename);	
		letters=Letters.letterGen();
	}
	/**
	 * Copy constructor
	 * @param currBoard BoardState instance to be copied
	 * @precondition A previous BoardState was instantiated
	 * @postcondition A deep copy of the BoardState passed in is created
	 */
	public BoardState(BoardState currBoard){
		board=new Tile[WIDTH][HEIGHT];
		for(int x=0;x<currBoard.board.length;x++){
			for(int y=0;y<currBoard.board[x].length;y++){
				board[x][y]=currBoard.board[x][y];
			}
		}
		word=currBoard.word;
		letters=Letters.letterGen();
	}
	/**
	 * 
	 * @param x Index of array for char to append
	 * @param y Index of array for char to append
	 * @precondition The BoardState has been instantiated
	 * @postcondition The character at the x,y coordinate is appended to the word being built
	 */
	public void addChar(int x,int y){
		word=word+board[x][y].getLetter();
	}
	
	/**
	 * 
	 * @param x Index of array to check if it has been visited
	 * @param y Index of array to check if it has been visited
	 * @return Boolean value indicating if that die in the board has been visited yet
	 * @precondition BoardState has been instantiated
	 * @postcondition A boolean is returned indicating if the search has been to this coordinate
	 */
	public boolean marked(int x,int y){
		if(board[x][y].getLetter()!=null)return true;
		return false;
	}
	/**
	 * @return String of the current word
	 * @precondition BoardState has been instantiated
	 * @postcondition The current word being built is returned
	 */
	public String getWord(){
		return word;
	}
	/**
	 * @precondition BoardState has been instantiated
	 * @postcondition The current board is printed to console
	 */
	public void printBoard(){
		for(int x=0;x<WIDTH;x++){
			for(int y=0;y<HEIGHT;y++){
				System.out.print(board[x][y].getLetter().getChar());
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
	}

	private Tile[][] readState(String filename){
		Tile[][] selected=new Tile[WIDTH][HEIGHT];
		try{
			Scanner scanner=new Scanner(new File(filename));
			int i=0;
			while(scanner.hasNextLine()&&i<HEIGHT){
				String line=scanner.nextLine();
				for(int x=0;x<WIDTH;x++){
					selected[i][x].setLetter(letters.getLetters()['a'-line.charAt(x)]);
				}
				i++;
			}
		}catch (IOException e){
			e.printStackTrace();
		}
		return selected;
	}
	private Tile[][] initBoard(){
		Tile[][] selected=new Tile[WIDTH][HEIGHT];

		for(int i=0;i<WIDTH;i++){
			for(int x=0;x<HEIGHT;x++){
				selected[i][x]=new Tile();
				//triple words
				if((i==0||i==14)&&(x==3||x==11))selected[i][x].setWordM(3);
				if((i==3||i==11)&&(x==0||x==14))selected[i][x].setWordM(3);
				//double words
				if((i==1||i==13)&&(x==5||x==9))selected[i][x].setWordM(2);
				if((i==5||i==9)&&(x==1||x==13))selected[i][x].setWordM(2);
				if((i==3||i==11)&&(x==7))selected[i][x].setWordM(2);
				if((i==7)&&(x==3||x==11))selected[i][x].setWordM(2);
				//triple letter				
				if((i==0||i==14)&&(x==6||x==8))selected[i][x].setCharM(3);
				if((i==6||i==8)&&(x==0||x==14))selected[i][x].setCharM(3);
				if((i==3||i==11)&&(x==3||x==11))selected[i][x].setCharM(3);
				if((i==5||i==9)&&(x==5||x==9))selected[i][x].setCharM(3);
				//double letter
				if((i==1||i==13)&&(x==2||x==12))selected[i][x].setCharM(2);
				if((i==2||i==12)&&(x==1||x==13))selected[i][x].setCharM(2);
				if((i==4||i==10)&&(x==2||x==12))selected[i][x].setCharM(2);
				if((i==2||i==12)&&(x==4||x==10))selected[i][x].setCharM(2);
				if((i==4||i==10)&&(x==6||x==8))selected[i][x].setCharM(2);
				if((i==6||i==8)&&(x==4||x==10))selected[i][x].setCharM(2);				
			}
			i++;
		}
		return selected;
	}

	public class Tile{
		private Letters.Letter l;
		private int wordM,charM;
		private boolean multUsed;
		
		public Tile(){
			l=null;
			wordM=charM=1;
			multUsed=false;
		}
		public Tile(Tile t){
			this.l=t.getLetter();
			this.charM=t.getCharM();
			this.wordM=t.getWordM();						
		}
		public boolean getMultUsed(){
			return multUsed;
		}
		public void setWordM(int wordM){
			this.wordM=wordM;
		}
		public void setCharM(int charM){
			this.charM=charM;
		}
		public int getCharM(){
			return charM;
		}
		public int getWordM(){
			return wordM;
		}
		public int getVal(){
			if(multUsed)return l.getVal();
			else{
				multUsed=true;
				return l.getVal()*wordM*charM;
			}
		}		
		public void setLetter(Letters.Letter l){
			this.l=l;
		}
		public Letters.Letter getLetter(){
			return l;
		}
	}
}
