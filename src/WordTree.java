import java.util.*;
import java.io.*;

public class WordTree
{
    private Node root = new Node();
    private class Node
    {
        String word;
        Node[] appendage = new Node[26];
    }

    private void preOrder(Node curr)
    {
        if(curr == null) return;
        if(curr.word != null) System.out.println(curr.word);
        for(Node n: curr.appendage) preOrder(n);
    }
    
    public void preOrder()
    {
        preOrder(root);
    }

    private void addWordToTree(String word, int x, Node n)
    {
        // if end of word, store word in n and return
        if(x==word.length())
        {
            n.word=word;
            return;
        }
        // check to see if branch on char x is already present
        if( n.appendage[word.charAt(x)-'A'] == null)
            // if it isn't present, make a new branch for char x
            n.appendage[word.charAt(x)-'A'] = new Node();
        // make recursive call that traverses that branch
        addWordToTree(word, x+1, n.appendage[word.charAt(x)-'A']);
    }

    public void addWordToTree(String word)
    {
        addWordToTree(word, 0, root);
    }

    public WordTree()
    {

    }
    
    public void addDict(String fname){
    	try {
    		Scanner scanner = new Scanner(new File(fname));
	
	        while(scanner.hasNext())
	        {
	            addWordToTree(scanner.next().toUpperCase());
	        }
    	}catch(FileNotFoundException e){
		// if not found in that location, check the parent directory
		// this is so it will work in eclipse and cli
		try {
		    Scanner scanner = new Scanner(new File("../"+fname));

		    while(scanner.hasNext())
		    {
			addWordToTree(scanner.next().toUpperCase());
		    }
		} catch(FileNotFoundException e2) {
		    e2.printStackTrace();
		}
    	}
    }
    /**
     * 
     * @param curr The current tree node
     * @param x Index of current place in the search
     * @param word The string being searched for
     * @return Boolean value indicating whether the string can still possibly be a word or not
     * @precondition The tree has been created
	 * @postcondition It is known whether the string can still be a word
     */
    public boolean checkValid(Node curr,int x,String word)
    {
        if(x==word.length())
        	return true;
        if( curr.appendage[word.charAt(x)-'A'] == null)
        	return false;
        
        return checkValid(curr.appendage[word.charAt(x)-'A'],x+1,word );
    }
    /**
     * 
     * @param curr The current tree node
     * @param x Index of current place in the search
     * @param word The string being searched for
     * @return Boolean value indicating whether a word is found in the tree
     * @precondition The tree has been created
	 * @postcondition It is known if the the word being searched for is in the tree or not
     */
    public boolean checkWord(Node curr, int x, String word){
    	if(curr==null)
    		return false;
    	if(x==word.length()){
    		if(curr.word==null)
    			return false;
            return curr.word.equals(word);
    	}
    	return checkWord(curr.appendage[word.charAt(x)-'A'],x+1,word);
    }
    /**
     * 
     * @return The root of the tree
     * @precondition The WordTree class has been instantiated
	 * @postcondition The root of the tree is returned
     */
    public Node getRoot(){
    	return root;
    }
}


