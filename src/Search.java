import java.util.ArrayList;
import java.util.Collections;

public class Search{
	String word;
	WordTree wt;
	State state;
	boolean done;
	ArrayList<String> words;
		
	public Search(String word,WordTree wt){
		this.word=word;
		this.wt=wt;
		char[] temp=word.toCharArray();
		state=new State(temp);
		done=false;
		words=new ArrayList<String>();
	}
	
	public void run(){
		search(state);
	}
		
	public void search(State state){
		if(done)return;
		if(wt.checkWord(wt.getRoot(), 0, state.getWord())){
			//System.out.println(state.getWord());
			String temp=state.getWord();
			if(!words.contains(temp))words.add(temp);
			if(state.currPos==(word.length()-1)){
				done=true;
				return;
			}			
		}			
		if(!wt.checkValid(wt.getRoot(),0,state.getWord())||state.currPos>(word.length()-1))
			return;
		if(state.firstPass)
			state.firstPass=false;
		else state.currPos++;
		for(int x=0;x<state.letters.length;x++){		
			if(!state.letters[x].isUsed&&state.currPos<state.letters.length){
				State newState=new State(state);
				newState.letters[x].isUsed=true;
				newState.currWord[newState.currPos]=newState.letters[x];					
				search(newState);
			}
		}		
	}
	
	public ArrayList<String> getWords(){
		Collections.sort(words);
		return words;
	}
	
	public class State{
		Letter[] letters,currWord;
		int currPos;
		boolean firstPass;
		
		public State(char[] temp){
			letters=new Letter[temp.length];
			currWord=new Letter[temp.length];
			for(int x=0;x<temp.length;x++){
				letters[x]=new Letter(temp[x],false);
			}
			currPos=0;
			firstPass=true;
		}
		
		public State(State state){
			this.letters=new Letter[state.letters.length];
			this.currWord=new Letter[state.letters.length];
			for(int x=0;x<state.letters.length;x++){
				this.letters[x]=new Letter(state.letters[x]);
			}
			for(int x=0;x<state.letters.length;x++){
				if(state.currWord[x]!=null){
					this.currWord[x]=new Letter(state.currWord[x]);
				}
			}
			currPos=state.currPos;
		}
		
		public String getWord(){
			StringBuilder sb=new StringBuilder();
			for(Letter l:currWord){
				if(l!=null){
					sb.append(l.letter);
				}
			}
			return sb.toString();
		}
		
		public String getAvailLetters(){
			StringBuilder sb=new StringBuilder();
			for(Letter l:letters){
				if(!l.isUsed){
					sb.append(l.letter);
				}
			}
			return sb.toString();
		}
		
		public class Letter{
			char letter;
			boolean isUsed;
			public Letter(char letter, boolean isUsed){
				this.letter=letter;
				this.isUsed=isUsed;
			}
			
			public Letter(Letter l){
				this.letter=l.letter;
				this.isUsed=l.isUsed;
			}
		}
	}
}