import java.util.Scanner;


public class WordsWithCheaters
{
	WordTree wordlist;

	public static void main(final String[] args)
	{
		final WordsWithCheaters t = new WordsWithCheaters();
		t.start();
	}

	public void start()
	{
		final String letters = getLetters();
		wordlist = new WordTree();
		for (char currLetter = 'a'; currLetter <= 'z'; currLetter++)
		{
			wordlist.addDict("words_" + currLetter + ".txt");
		}
		System.out.println("word list populated");
		final Search toSearch = new Search(letters.toUpperCase(), wordlist);
		toSearch.run();
		for (final String s : toSearch.getWords())
		{
			System.out.println(s);
		}
	}

	public String getLetters()
	{
		System.out.println("Enter your letters:");
		final Scanner scan = new Scanner(System.in);
		return scan.next();
	}
}
